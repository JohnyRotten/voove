class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true
  belongs_to :user
  has_many :likes, as: :likable, dependent: :destroy
  has_many :dislikes, as: :dislikable, dependent: :destroy

  validates :content, presence: true, length: { maximum: 140 }
end

class User < ActiveRecord::Base

  has_many :articles
  has_many :comments
  has_many :likes, dependent: :destroy
  has_many :dislikes, dependent: :destroy

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  def article_owner?(article)
    articles.include? article
  end

  def like(type, id)
    self.likes.find_by(likable_id: id, likable_type: type)
  end

  def has_like?(type, id)
    like(type, id).present?
  end

  def dislike(type, id)
    self.dislikes.find_by(dislikable_id: id, dislikable_type: type)
  end

  def has_dislike?(type, id)
    dislike(type, id).present?
  end

  def image_path(size = 200)
    "http://www.gravatar.com/avatar/#{Digest::MD5.hexdigest email.downcase}?s=#{size}"
  end

end

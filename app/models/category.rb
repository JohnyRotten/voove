class Category < ActiveRecord::Base
  belongs_to :category
  has_many :categories
  has_many :articles

  validates :name, presence: true, length: { in: 3..20 }
  validates :description, length: { maximum: 75 }

  def Category.tree
    Category.find_by_category_id 0
  end
end

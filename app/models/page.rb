class Page < ActiveRecord::Base
  has_many :comments, as: :commentable, dependent: :destroy

  validates :title, presence: true, uniqueness: { case_sensetive: false }
  validates :description, presence: true, length: { in: 5..140 }
  validates :body, presence: true
end

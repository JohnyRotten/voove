class Article < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :likes, as: :likable, dependent: :destroy
  has_many :dislikes, as: :dislikable, dependent: :destroy

  validates :title, presence: true, length: { in: 5..20 }
  validates :description, presence: true, length: { maximum: 75 }


end

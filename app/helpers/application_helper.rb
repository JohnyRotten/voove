module ApplicationHelper
  def full_title(title)
    base_title = 'VooVe'
    return base_title if title.nil?
    return "#{base_title} | #{title}" if title.present?
  end
end

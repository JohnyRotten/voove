class DislikesController < ApplicationController

  before_action :authenticate_user!

  def create
    @dislike = current_user.dislikes.create(dislike_params)
    respond_to do |format|
      if @dislike.save
        format.js {}
        format.json { render json: @dislike, status: :create, location: @dislike }
      else
        format.json { render json: @dislike.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    dislike = current_user.dislikes.find(params[:id])
    if dislike.present?
      id, type = dislike.dislikable.id, dislike.dislikable.class
      dislike.destroy
      @dislike = Dislike.new
      @dislike.dislikable_id = id
      @dislike.dislikable_type = type
      respond_to do |format|
        format.js {}
      end
    end
  end

  private

  def dislike_params
    params.require(:dislike).permit(:dislikable_id, :dislikable_type)
  end

end

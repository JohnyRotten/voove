class ArticlesController < ApplicationController

  before_action :authenticate_user!, only: [ :new, :create, :edit, :update, :destroy ]
  before_action :article_owner!, only: [ :edit, :update, :destroy ]

  def index
    @articles = Article.paginate(page: params[:page])
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = current_user.articles.new
  end

  def create
    @article = current_user.articles.create(article_params)
    if @article.save
      flash[:green] = t('articles.saved')
      redirect_to @article
    else
      flash[:red] = t('articles.not_saved')
      render :new
    end
  end

  def edit
  end

  def update
    if @article.update_attributes article_params
      flash[:green] = t('articles.updated')
      redirect_to @article
    else
      flash[:red] = t('articles.not_updates')
      render :edit
    end
  end

  def destroy
    @article.destroy
    flash[:green] = t('articles.destroyed')
    redirect_to articles_path
  end

  private

    def article_params
      params.require(:article).permit(:title, :description, :body, :category_id)
    end

    def article_owner!
      @article = current_user.articles.find(params[:id])
      if @article.nil?
        flash[:red] = t('articles.no_owner')
        redirect :back
      end
    end

end

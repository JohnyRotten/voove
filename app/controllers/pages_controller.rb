class PagesController < ApplicationController
  before_action :authenticate_user!, only: [ :index, :new, :create, :edit, :update, :destroy ]
  before_action :admin_user!, only: [ :index, :new, :create, :edit, :update, :destroy ]

  def index
    @pages = Page.paginate(page: params[:page])
  end

  def show
    @page = Page.find(params[:id])
  end

  def show_by_name
    @page = Page.find_by_title(params[:page]) or not_found
    render 'show' if @page.present?
  end

  def new
    @page = Page.new
  end

  def create
    @page = Page.create(page_params)
    if @page.save
      flash[:green] = t('pages.create')
      redirect_to @page
    else
      flash[:red] = t('pages.not_create')
      render 'new'
    end
  end

  def edit
    @page = Page.find(params[:id])
  end

  def update
    @page = Page.find(params[:id])
    if @page.update_attributes page_params
      flash[:green] = t('pages.updated')
      redirect_to @page
    else
      flash[:red] = t('pages.not_updated')
      render 'edit'
    end
  end

  def destroy
    Page.find(params[:id]).destroy
    flash[:green] = t('pages.destroyed')
    redirect_to :back
  end

  private

    def page_params
      params.require(:page).permit :title, :description, :body, :meta_title, :meta_keywords, :meta_description, :enabled, :commentabled
    end

    def admin_user!
      unless current_user.admin?
        flash[:red] = t('pages.not_admin')
        redirect_to :back
      end
    end

end

class CommentsController < ApplicationController

  before_action :authenticate_user!

  def create
    @comment = current_user.comments.create(comment_params)
    if @comment.save
      flash[:green] = t('comments.create')
      redirect_to @comment.commentable
    else
      flash[:red] = t('comments.not_create')
      redirect_to :back
    end
  end

  def destroy
    Comment.find(params[:id]).destroy!
    flash[:green] = t('comments.destroyed')
    redirect_to :back
  end

  private

    def comment_params
      params.require(:comment).permit(:content, :commentable_type, :commentable_id)
    end

end

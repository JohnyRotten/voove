class CategoriesController < ApplicationController

  before_action :authenticate_user!, only: [ :index, :create, :update, :destroy ]
  before_action :admin_user!, only: [ :index, :create, :update, :destroy ]

  def index
    @tree = Category.tree
  end

  def show
    @category = Category.find(params[:id])
  end

  def create
    category = Category.create(category_params)
    if category.save
      flash[:green] = t('categories.created')
    else
      flash[:red] = t('categories.not_created')
    end
    redirect_to :back
  end

  def update
    category = Category.find(params[:id])
    if category.update_attributes category_params
      flash[:green] = t('categories.updated')
    else
      flash[:red] = t('categories.not_updated')
    end
  end

  def destroy
    Category.find(params[:id]).destroy
    flash[:green] = t('categories.destroyed')
    redirect_to :back
  end

  private

    def category_params
      params.require(:category).permit(:name, :description, :category_id, :enabled)
    end

    def admin_user!
      unless current_user.admin?
        flash[:red] = t('categories.not_admin')
        redirect_to :back
      end
    end

end

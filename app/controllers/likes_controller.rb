class LikesController < ApplicationController

  before_action :authenticate_user!

  def create
    @like = current_user.likes.create(like_params)
    respond_to do |format|
      if @like.save
        format.js {}
        format.json { render json: @like, status: :create, location: @like }
      else
        format.json { render json: @like.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    like = current_user.likes.find(params[:id])
    if like.present?
      id, type = like.likable.id, like.likable.class
      like.destroy
      @like = Like.new
      @like.likable_id = id
      @like.likable_type = type
      respond_to do |format|
        format.js {}
      end
    end
  end

  private

    def like_params
      params.require(:like).permit(:likable_id, :likable_type)
    end

end

class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.integer :user_id, null: false
      t.text :body, null: false
      t.integer :category_id
      t.timestamps null: false
    end
    add_index :articles, [ :title, :user_id ], unique: true
    add_index :articles, :category_id
    add_index :articles, :user_id
  end
end

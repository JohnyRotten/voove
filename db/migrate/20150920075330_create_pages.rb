class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title, unique: true
      t.text :description
      t.text :body
      t.string :meta_title, default: ''
      t.string :meta_description, default: ''
      t.string :meta_keywords, default: ''
      t.boolean :enabled, default: false
      t.boolean :commentabled, default: false
      t.timestamps null: false
    end
  end
end

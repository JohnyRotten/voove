class CreateDislikes < ActiveRecord::Migration
  def change
    create_table :dislikes do |t|
      t.integer :user_id, null: false, index: true
      t.references :dislikable, polymorphic: true
      t.timestamps null: false
    end
  end
end

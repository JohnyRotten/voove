Rails.application.routes.draw do

  root 'home#index'

  match '/about', to: 'home#about', via: 'get'
  match '/contacts', to: 'home#contacts', via: 'get'

  devise_for :users
  resources :users, only: [ :index, :show ]

  resources :articles, :pages
  resources :comments, only: [ :create, :destroy ]
  resources :likes, only: [ :create, :destroy ]
  resources :dislikes, only: [ :create, :destroy ]

  resources :categories, only: [ :show, :create, :update, :destroy ]

  match '/admin/categories', to: 'categories#index', via: 'get'

  match '/:page', to: 'pages#show_by_name', via: 'get'

end
